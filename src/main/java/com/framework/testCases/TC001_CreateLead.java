package com.framework.testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "Create a new Lead";
		testNodes = "Leads";
		author = "Sathish";
		category = "Smoke";
		dataSheetName = "UseCase1";
	}
	
	@Test(dataProvider = "fetchData")
	public void createlead(String username, String password, String cName, String fName, String lName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLead()
		.clickCreateLead()
		.enterCompName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.verifyLead(cName);
	}

}
