package com.framework.testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_DuplicateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_DupLead";
		testDescription = "Duplicate an existing Lead";
		testNodes = "Leads";
		author = "Sathish";
		category = "Smoke";
		dataSheetName = "UseCase1";
	}
	
	@Test(dataProvider = "fetchData")
	public void duplicateLead(String username, String password, String cName, String fName, String lName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLead()
		.clickFindLead()
		.enterFirstName(fName)
		.clickFindLeadButton()
		.clickFirstResult()
		.verifyPageTitle("View Lead | opentaps CRM")
		.clickDupButton()
		.verifyPageTitle("Duplicate Lead | opentaps CRM")
		.clickCreateLeadButton()
		.verifyPageTitle("View Lead | opentaps CRM")
		.verifyLead(cName);
		
	
	}
}
