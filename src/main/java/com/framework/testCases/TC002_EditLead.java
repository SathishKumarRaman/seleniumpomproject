package com.framework.testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_EditLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_EditLead";
		testDescription = "Edit an existing Lead";
		testNodes = "Leads";
		author = "Sathish";
		category = "Smoke";
		dataSheetName = "UseCase1";
	}
	
	@Test(dataProvider = "fetchData")
	public void editLead(String username, String password, String cName, String fName, String lName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCrmsfa()
		.clickLead()
		.clickFindLead()
		.enterFirstName(fName)
		.clickFindLeadButton()
		.clickFirstResult()
		.verifyPageTitle("View Lead | opentaps CRM")
		.clickEditButton()
		.enterCompName(cName)
		.clickUpdateButton()
		.verifyLead(cName);
	
	}

}
