package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompName;
	public CreateLeadPage enterCompName(String data) {
		clearAndType(eleCompName, data);
		return this;
	}
	
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	@FindBy(how = How.NAME,using="submitButton") WebElement eleCreateLeadButton;
	public ViewLeadsPage clickCreateLeadButton() {
		click(eleCreateLeadButton);
		return new ViewLeadsPage();
	}

}
















