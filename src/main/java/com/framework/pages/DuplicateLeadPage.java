package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{

	public DuplicateLeadPage() {
       PageFactory.initElements(driver, this);
	}
	
	public DuplicateLeadPage verifyPageTitle(String data) {
    	verifyTitle(data);
    	return this;
    }
	
	@FindBy(how = How.NAME,using="submitButton") WebElement eleCreateLeadButton;
    public ViewLeadsPage clickCreateLeadButton() {
    	click(eleCreateLeadButton);
    	return new ViewLeadsPage();
    }
}



















