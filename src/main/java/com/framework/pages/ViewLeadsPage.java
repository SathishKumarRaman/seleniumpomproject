package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{

	public ViewLeadsPage() {
       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="viewLead_companyName_sp") WebElement elefName;
    public ViewLeadsPage verifyLead(String data) {
    	verifyPartialText(elefName, data);
    	return this;
    }
    
    public ViewLeadsPage verifyPageTitle(String data) {
    	verifyTitle(data);
    	return this;
    }
    
    @FindBy(how = How.XPATH,using="//a[text()='Edit']") WebElement eleEditButton;
    public UpdateLeadPage clickEditButton() {
    	click(eleEditButton);
    	return new UpdateLeadPage();
    }
    
    @FindBy(how = How.XPATH,using="//a[@class='subMenuButton']") WebElement eleDupButton;
    public DuplicateLeadPage clickDupButton() {
    	click(eleDupButton);
    	return new DuplicateLeadPage();
    }
}



















