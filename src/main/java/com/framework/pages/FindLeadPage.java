package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFName;
	public FindLeadPage enterFirstName(String data) {
		clearAndType(eleFName, data);
		return this;
	}
	
	@FindBy(how = How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLead;
	public FindLeadPage clickFindLeadButton() {
		click(eleFindLead);
		return this;
	}
	
	
	@FindBy(how = How.XPATH,using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a") WebElement eleFirstResult;
	public ViewLeadsPage clickFirstResult() {
		click(eleFirstResult);
		return new ViewLeadsPage();
	}
}













