package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class UpdateLeadPage extends ProjectMethods{

	public UpdateLeadPage() {
       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="updateLeadForm_companyName") WebElement eleCName;
    public UpdateLeadPage enterCompName(String data) {
    	clearAndType(eleCName, data);
    	return this;
    }
    
	@FindBy(how = How.NAME,using="submitButton") WebElement eleUpdateButton;
    public ViewLeadsPage clickUpdateButton() {
    	click(eleUpdateButton);
    	return new ViewLeadsPage();
    }
}



















